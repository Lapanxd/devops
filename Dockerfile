FROM php:8.1-apache

RUN apt-get update \
    && apt-get install -y libgs9-common \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

ADD src /var/www/html
EXPOSE 80
